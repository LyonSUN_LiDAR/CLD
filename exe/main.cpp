#include <iostream>

#include "../lib_CLD/IO.h"


int main(int argc , char **argv)
{

    std::cout   << "\n"
                << "***************************************\n"
                << "*                                     *\n"
                << "*    Local Descriptors Computation    *\n"
                << "*                                     *\n"
                << "***************************************"
                << "\n\n"
                << "This exe is based on CLD library."
                << "\n\n"
                << "Its main goal is to compute 4 descriptors "
                << "based on local neighborhood of optimal size."
                << "\n\n"
                << "These descriptors are "
                << "\n\tLinearity"
                << "\n\tPlanarity"
                << "\n\tScattering"
                << "\n\tVerticality"
                << "\n\n"
                << "For more details, please refer to: \n"
                << "Guinard, S. and Landrieu, L., 2017\n"
                << "Weakly supervised segmentation-aided classification of urban scenes from 3D LiDAR point clouds\n"
                << "https://www.int-arch-photogramm-remote-sens-spatial-inf-sci.net/XLII-1-W1/151/2017/isprs-archives-XLII-1-W1-151-2017.pdf\n\n"
                << std::endl;

    if (argc < 2 || argc > 5)
    {
        std::cout   << "Wrong number of arguments: \n"
                    << "\targ1 = input point cloud\n"
                    << "\targ2 = min. number of neighbors (optional, default = 15)\n"
                    << "\targ3 = max. number of neighbors (optional, default = 50)\n"
                    << "\targ4 = step between neighborhood sizes (optional, default = 5)\n"
                    << std::endl;
        return 1;
    }

    int i_arg = 1;
    int kmin = 15, kmax = 50, kstep = 5;

    //io
    std::string file = std::string(argv[1]);
    if (argc > i_arg) kmin  = std::atoi(argv[i_arg++]);
    if (argc > i_arg) kmax  = std::atoi(argv[i_arg++]);
    if (argc > i_arg) kstep = std::atoi(argv[i_arg++]);

    ComputeLocalDescriptors(file,kmin,kmax,kstep);

    return 0;
}
