#ifndef __ANN_LIDAR_ACCESS_H__
#define __ANN_LIDAR_ACCESS_H__


#include <ANN/ANN.h> // ANN declarations
#include "../LidarClassifStuff/List/ListCoord.h"
//#include "../LidarClassifStuff/List/ListKanade.h"
//#include "../LidarClassifStuff/List/ListSegment3d.h"


class ANNLidarAccess
{
    ListCoord* mp_coord;
public :
ANNLidarAccess(ListCoord* p_coord):mp_coord(p_coord){}

void operator()(Lidar::LidarDataContainer::iterator it, ANNpoint p)// read point
{
        p[0] = mp_coord->x.Value(it);
        p[1] = mp_coord->y.Value(it);
        p[2] = mp_coord->z.Value(it);
}
};

class ANNLidarAccessPoint2D
{
    ListCoord* mp_coord;
public :
ANNLidarAccessPoint2D(ListCoord* p_coord):mp_coord(p_coord){}

void operator()(Lidar::LidarDataContainer::iterator it, ANNpoint p)// read point
{
        p[0] = mp_coord->x.Value(it);
        p[1] = mp_coord->y.Value(it);
}
};

/*class ANNLidarAccessKanade
{
    ListKanade* mp_kanade;
public :
ANNLidarAccessKanade(ListKanade* p_kanade):mp_kanade(p_kanade){}

void operator()(Lidar::LidarDataContainer::iterator it, ANNpoint p)// read point
{
        p[0] = mp_kanade->kan_theta.Value(it);
        p[1] = mp_kanade->kan_phi  .Value(it);
}
};*/

template<typename t_type>
class TemplateANNLidarAccess3D
{
    TAttribute<t_type>* mpx;
    TAttribute<t_type>* mpy;
    TAttribute<t_type>* mpz;
public :
    TemplateANNLidarAccess3D(TAttribute<t_type>* px, TAttribute<t_type>* py, TAttribute<t_type>* pz):mpx(px), mpy(py), mpz(pz){}

void operator()(Lidar::LidarDataContainer::iterator it, ANNpoint p)// read point
{
        p[0] = mpx->Value(it);
        p[1] = mpy->Value(it);
        p[2] = mpz->Value(it);
}
};

class ANNLidarAccess3D
{
    TAttribute<float>* mpx;
    TAttribute<float>* mpy;
    TAttribute<float>* mpz;
public :
    ANNLidarAccess3D(TAttribute<float>* px, TAttribute<float>* py, TAttribute<float>* pz):mpx(px), mpy(py), mpz(pz){}

void operator()(Lidar::LidarDataContainer::iterator it, ANNpoint p)// read point
{
        p[0] = mpx->Value(it);
        p[1] = mpy->Value(it);
        p[2] = mpz->Value(it);
}
};

class ANNLidarAccess2D
{
    TAttribute<float>* mpi;
    TAttribute<float>* mpj;
public :
    ANNLidarAccess2D(TAttribute<float>* pi, TAttribute<float>* pj):mpi(pi), mpj(pj){}

void operator()(Lidar::LidarDataContainer::iterator it, ANNpoint p)// read point
{
        p[0] = mpi->Value(it);
        p[1] = mpj->Value(it);
}
};

class ANNLidarAccess6D
{
    TAttribute<float>* mpx;
    TAttribute<float>* mpy;
    TAttribute<float>* mpz;
    TAttribute<float>* mpu;
    TAttribute<float>* mpv;
    TAttribute<float>* mpw;
public :
    ANNLidarAccess6D(TAttribute<float>* px, TAttribute<float>* py, TAttribute<float>* pz,
                     TAttribute<float>* pu, TAttribute<float>* pv, TAttribute<float>* pw):
        mpx(px), mpy(py), mpz(pz), mpu(pu), mpv(pv), mpw(pw){}

void operator()(Lidar::LidarDataContainer::iterator it, ANNpoint p)// read point
{
        p[0] = mpx->Value(it);
        p[1] = mpy->Value(it);
        p[2] = mpz->Value(it);
        p[3] = mpu->Value(it);
        p[4] = mpv->Value(it);
        p[5] = mpw->Value(it);
}
};

/*class ANNLidarAccessSegment3D
{
    ListSegment3d* mp;
    float m_coeff_direction;

public :
    ANNLidarAccessSegment3D(ListSegment3d* list, float coeff_direction = 1.):
        mp(list),
        m_coeff_direction(coeff_direction)
    {}

void operator()(Lidar::LidarDataContainer::iterator it, ANNpoint p)// read point
{
    const Lg::Point3f a(mp->AXYZ(it));
    const Lg::Point3f b(mp->BXYZ(it));

    const Lg::Point3f mid(Lg::midpoint(a, b));

        p[0] = mid.X();
        p[1] = mid.Y();
        p[2] = mid.Z();


    Lg::Point3f vect(b - a);

    const float norm2 = vect.Norm2();
    //const float norm2 = vect.X() * vect.X() + vect.Y() * vect.Y();

    if(norm2 == 0)
    {
        p[3] = 0;
        p[4] = 0;
        p[5] = 0;
    }
    else
    {
        vect = vect * (m_coeff_direction / sqrt(norm2));

        p[3] = vect.X();
        p[4] = vect.Y();
        p[5] = vect.Z();
    }

}

};*/


#endif //__ANN_LIDAR_ACCESS_H__
