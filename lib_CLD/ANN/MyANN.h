#ifndef __MY_ANN_H__
#define __MY_ANN_H__

#include <cstdlib> // C standard library
#include <limits>
#include <time.h>
#include <stdio.h>
#include <fstream> // file I/O
#include <ANN/ANN.h> // ANN declarations


#include "NearestNeighbors.h"

using namespace std; // make std:: accessible

//void getArgs(int argc, char **argv) { ... } // get command-line arguments

//bool readPt(istream& in, ANNpoint p) { ... } // read point (false on EOF)



/*
*/
class MyANN
{
    const int dim;
	double eps; 
	//istream* dataIn = NULL;
	//istream* queryIn = NULL;
    int nPts; // actual number of data points
    ANNpointArray dataPts; // data points
public :

    ANNpoint queryPt; // query point
    ANNkd_tree* kdTree; // search structure

MyANN(const int dim, const int maxPts):
    dim(dim),
    queryPt ( annAllocPt(dim)), // allocate query point
    dataPts ( annAllocPts(maxPts, dim)), // allocate data points
    nPts (0) // number of points in the kdTree
{}

~MyANN()
{ 
    delete kdTree;
    annClose(); // done with ANN
}

int Size()const{return nPts;}

template< class t_iterator, class t_fill_point >
void BuildTree(const t_iterator first, const t_iterator last, t_fill_point& fill_point)
{
	// read data points
    //while (nPts < maxPts && readPt(*dataIn, dataPts[nPts])) nPts++;
    
    //std::cout << "fill_points" << std::endl;
    nPts=0;
    for(t_iterator it = first; it != last; ++it, ++nPts) fill_point(it, dataPts[nPts]);
    
    //std::cout << "Build search structure." << std::endl;
    kdTree = new ANNkd_tree(dataPts, nPts, dim);
}

ANNpoint&  QueryPoint()
{
    return queryPt;
}

/// @param eps : error bound
/// @attention : On suppose que query point (QueryPoint()) a ete initialise
void Query(NearestNeighbors& nn, const double eps = 0)
{
        // search
        kdTree->annkSearch
                (
                queryPt,         // query point
                nn.k,            // number of near neighbors
                nn.idx,          // nearest neighbors (returned)
                nn.squared_dists,// distance (returned)
                eps              // error bound
                );
}

/// @param eps : error bound
template< class t_iterator, class t_fill_point >
void Query(t_iterator& it, t_fill_point& fill_point, NearestNeighbors& nn, const double eps = 0)
{
	    // read query points
        fill_point(it, queryPt);
        // search
        Query(nn, eps);
}

};

#endif //__MY_ANN_H__
