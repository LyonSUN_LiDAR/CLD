#ifndef __MY_INDEXATION_H__
#define __MY_INDEXATION_H__

#include "MyANN.h"
#include "ANNLidarAccess.h"

#include "../LidarClassifStuff/List/ListCoord.h"
#include "../LidarClassifStuff/List/ListKNearestNeighbors.h"
#include "../LidarClassifStuff/ContainerTools.h"


using namespace std; // make std:: accessible

//void getArgs(int argc, char **argv) { ... } // get command-line arguments

//bool readPt(istream& in, ANNpoint p) { ... } // read point (false on EOF)

/*
*/

template< class t_fill_point >
void NeighborsStorage(ListKNearestNeighbors& lknn, t_fill_point& fill_point, const int dim, const double eps=0)
{
    const int k = lknn.neighbor.size();

    MyANN myAnn(dim, lknn.LidarDataContainer().size());

    const Lidar::LidarDataContainer::iterator begin = lknn.Begin();
    const Lidar::LidarDataContainer::iterator end   = lknn.End();

    myAnn.BuildTree(begin, end, fill_point);

    NearestNeighbors nn(k);

    for(Lidar::LidarDataContainer::iterator it = begin; it != end; ++ it)
    {
        myAnn.Query(it, fill_point, nn, eps);

        // storage
        for(int i=0; i != k; ++i) lknn.neighbor.at(i).Value(it) = nn.idx[i];

        // unsquare distance
        /*
				for (int i = 0; i < k; i++)
				{
					dists[i] = sqrt(dists[i]);
					cout << i << " " << nnIdx[i] << " " << dists[i] << "\n";
				}
				*/
    }
}

void MyIndexation( 	const std::string& nomfic_in,
                        const std::string& nomfic_neigh,
                        const int k,
                        const int dim=3,
                        const double eps=0);

#endif //__MY_INDEXATION_H__
