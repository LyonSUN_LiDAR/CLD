/// @file NearestNeighbors.h

#ifndef __NEAREST_NEIGHBORS_H__
#define __NEAREST_NEIGHBORS_H__

#include <ANN/ANN.h> // ANN declarations

struct NearestNeighbors
{
    int k;
    ANNidxArray  idx;           // near neighbor indices
    ANNdistArray squared_dists; // near neighbor distances

    NearestNeighbors(){}

    void Init(const int k)
    {
        this->k = k;
        idx           = new ANNidx[k]; // allocate near neigh indices
        squared_dists = new ANNdist[k]; // allocate near neighbor dists
    }

    NearestNeighbors(const int k):
        k(k),
        idx           (new ANNidx[k]), // allocate near neigh indices
        squared_dists (new ANNdist[k]) // allocate near neighbor dists
        {}

    ~NearestNeighbors()
    {
        delete [] idx; // clean things up
        delete [] squared_dists;
    }
};

#endif //__NEAREST_NEIGHBORS_H__
