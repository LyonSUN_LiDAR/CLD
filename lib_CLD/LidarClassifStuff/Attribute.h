#ifndef __ATTRIBUTE_H__
#define __ATTRIBUTE_H__

#include "LidarFormat/LidarDataContainer.h"
#include "LidarFormat/LidarDataFormatTypes.h"

#include <vector>
#include "LidarFormat/LidarFile.h"
#include "LidarFormat/LidarEcho.h"


class Attribute
{
public:

    enum io_style_t{in, out, in_out};

    std::string name;
    Lidar::LidarDataType lidarDataType;
    unsigned int shift;


Attribute():
    name(""),
    lidarDataType(Lidar::LidarDataType::float32),
    shift(0)
{}

Attribute(
        const std::string arg_name,
        Lidar::LidarDataType arg_lidarDataType,
        unsigned int arg_shift
        ):
    name(arg_name),
    lidarDataType(arg_lidarDataType),
    shift(arg_shift)
{}

void GetDecalage(Lidar::LidarDataContainer& ldc){shift = ldc.getDecalage(name);}

void AddToContainer(Lidar::LidarDataContainer& ldc, enum io_style_t io_style)
{
    if(!CheckName()) return;

    if(ldc.checkAttributeIsPresent(name))
    {
        if(io_style == out)
        {
            std::cout << "WARNING: Attribute " << name <<" is already in the container" << std::endl;
        }
        else if(!ldc.checkAttributeIsPresentAndType(name, lidarDataType))
        {
            std::cout << "WARNING: Not adding attribute " << name <<" because its type "<<
                         Lidar::Name(ldc.getAttributeType(name)) << "!=" << Lidar::Name(lidarDataType) << std::endl;
            return;
        }
    }
    else
    {
        if(io_style == in)
        {
            std::cout << "WARNING: No attribute " << name <<" in the container" << std::endl;
            return;
        }
    }
    ldc.addAttribute(name,lidarDataType);
    GetDecalage(ldc);
}

// BV: more comprehensible name
void GetFromContainer(Lidar::LidarDataContainer& ldc)
{
    if(!CheckName()) return;

    if(!ldc.checkAttributeIsPresent(name))
    {
        std::cout << "WARNING: No attribute " << name <<" in the container" << std::endl;
        return;
    }
    if(!ldc.checkAttributeIsPresentAndType(name, lidarDataType))
    {
        std::cout << "WARNING: Not adding attribute " << name <<" because its type "<<
                     Lidar::Name(ldc.getAttributeType(name)) << "!=" << Lidar::Name(lidarDataType) << std::endl;
        return;
    }
    ldc.addAttribute(name,lidarDataType);
    GetDecalage(ldc);
}

bool CheckName()
{
    if(name != "") return true;
    std::cout << "WARNING: Attribute has no name" << std::endl;
    return false;
}

void Copy(Lidar::LidarDataContainer& ldc, Attribute& attribute_model)
{
    Copy(ldc,ldc,attribute_model);
}

void Copy(Lidar::LidarDataContainer& ldc_copy, Lidar::LidarDataContainer& ldc_model, Attribute& attribute_model)
{
    const Lidar::LidarDataContainer::iterator it_copy_end = ldc_copy.end();
    const Lidar::LidarDataContainer::iterator it_model_end = ldc_model.end();

    Lidar::LidarDataContainer::iterator it_copy = ldc_copy.begin();
    Lidar::LidarDataContainer::iterator it_model = ldc_model.begin();

    if(lidarDataType == Lidar::LidarDataType::float32)
    {
        for(;it_copy!=it_copy_end, it_model!=it_model_end; ++it_copy, ++it_model){ it_copy.value<float>(shift) = it_model.value<float>(attribute_model.shift); }
    }
    else if(lidarDataType == Lidar::LidarDataType::float64)
    {
        for(;it_copy!=it_copy_end, it_model!=it_model_end; ++it_copy, ++it_model){ it_copy.value<double>(shift) = it_model.value<double>(attribute_model.shift); }
    }
    else if(lidarDataType == Lidar::LidarDataType::int8)
    {
        for(;it_copy!=it_copy_end, it_model!=it_model_end; ++it_copy, ++it_model){ it_copy.value<char>(shift) = it_model.value<char>(attribute_model.shift); }
    }
    else if(lidarDataType == Lidar::LidarDataType::int16)
    {
        for(;it_copy!=it_copy_end, it_model!=it_model_end; ++it_copy, ++it_model){ it_copy.value<short>(shift) = it_model.value<short>(attribute_model.shift); }
    }
    else if(lidarDataType == Lidar::LidarDataType::int32)
    {
        for(;it_copy!=it_copy_end, it_model!=it_model_end; ++it_copy, ++it_model){ it_copy.value<int>(shift) = it_model.value<int>(attribute_model.shift); }
    }
    else if(lidarDataType == Lidar::LidarDataType::int64)
    {
        for(;it_copy!=it_copy_end, it_model!=it_model_end; ++it_copy, ++it_model){ it_copy.value<long>(shift) = it_model.value<long>(attribute_model.shift); }
    }
    else if(lidarDataType == Lidar::LidarDataType::uint8)
    {
        for(;it_copy!=it_copy_end, it_model!=it_model_end; ++it_copy, ++it_model){ it_copy.value<unsigned char>(shift) = it_model.value<unsigned char>(attribute_model.shift); }
    }
    else if(lidarDataType == Lidar::LidarDataType::uint16)
    {
        for(;it_copy!=it_copy_end, it_model!=it_model_end; ++it_copy, ++it_model){ it_copy.value<unsigned short>(shift) = it_model.value<unsigned short>(attribute_model.shift); }
    }
    else if(lidarDataType == Lidar::LidarDataType::uint32)
    {
        for(;it_copy!=it_copy_end, it_model!=it_model_end; ++it_copy, ++it_model){ it_copy.value<unsigned int>(shift) = it_model.value<unsigned int>(attribute_model.shift); }
    }
    else if(lidarDataType == Lidar::LidarDataType::uint64)
    {
        for(;it_copy!=it_copy_end, it_model!=it_model_end; ++it_copy, ++it_model){ it_copy.value<unsigned long>(shift) = it_model.value<unsigned long>(attribute_model.shift); }
    }
    else
    {
        std::cout << "Attribute::Copy : unknown attribute type" << std::endl;
    }
}

void Copy(Lidar::LidarDataContainer::iterator it, Attribute& attribute_model)
{
    Copy(it, it, attribute_model);
}

void Copy(Lidar::LidarDataContainer::iterator it_copy, Lidar::LidarDataContainer::iterator it_model, Attribute& attribute_model)
{
    if(lidarDataType == Lidar::LidarDataType::float32)        { it_copy.value<float>(shift) = it_model.value<float>(attribute_model.shift); }
    else if(lidarDataType == Lidar::LidarDataType::float64)   { it_copy.value<double>(shift) = it_model.value<double>(attribute_model.shift); }
    else if(lidarDataType == Lidar::LidarDataType::int8)      { it_copy.value<char>(shift) = it_model.value<char>(attribute_model.shift); }
    else if(lidarDataType == Lidar::LidarDataType::int16)     { it_copy.value<short>(shift) = it_model.value<short>(attribute_model.shift); }
    else if(lidarDataType == Lidar::LidarDataType::int32)     { it_copy.value<int>(shift) = it_model.value<int>(attribute_model.shift); }
    else if(lidarDataType == Lidar::LidarDataType::int64)     { it_copy.value<long>(shift) = it_model.value<long>(attribute_model.shift); }
    else if(lidarDataType == Lidar::LidarDataType::uint8)     { it_copy.value<unsigned char>(shift) = it_model.value<unsigned char>(attribute_model.shift); }
    else if(lidarDataType == Lidar::LidarDataType::uint16)    { it_copy.value<unsigned short>(shift) = it_model.value<unsigned short>(attribute_model.shift); }
    else if(lidarDataType == Lidar::LidarDataType::uint32)    { it_copy.value<unsigned int>(shift) = it_model.value<unsigned int>(attribute_model.shift); }
    else if(lidarDataType == Lidar::LidarDataType::uint64)    { it_copy.value<unsigned long>(shift) = it_model.value<unsigned long>(attribute_model.shift); }
}

};

#endif // __ATTRIBUTE_H__
