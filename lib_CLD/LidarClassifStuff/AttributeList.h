#ifndef __ATTRIBUTE_LIST_H__
#define __ATTRIBUTE_LIST_H__

#include "Attribute.h"

class AttributeList
{
protected :

    Lidar::LidarDataContainer::iterator m_it_point;

    std::vector< Attribute* > mp_attributes;
    Lidar::LidarDataContainer* m_lidar_data_container;

public:

    AttributeList(){}

    AttributeList(Lidar::LidarDataContainer& ldc, enum Attribute::io_style_t io_style)
    {
        AddToContainer(ldc, io_style);
    }

    bool CheckAttributesArePresent(Lidar::LidarDataContainer& ldc)
    {
        bool checking = true;
        for(std::vector< Attribute* >::iterator it = mp_attributes.begin(); checking && it != mp_attributes.end(); ++it)
        {
            checking = checking && ldc.checkAttributeIsPresent((*it)->name);
        }
        return checking;
    }

    bool CheckAttributesArePresentAndType(Lidar::LidarDataContainer& ldc)
    {
        bool checking = true;
        for(std::vector< Attribute* >::iterator it = mp_attributes.begin(); checking && it != mp_attributes.end(); ++it)
        {
            checking = checking && ldc.checkAttributeIsPresentAndType((*it)->name, (*it)->lidarDataType);
        }
        return checking;
    }


    /// @attention : bien faire attention a changer le nom des attributs avant de les ajouter dans le container (pas apres)
    ///              peut avoir des problemes avec cette fonction a cause de pointeurs
    void AddPrefixe(const std::string prefixe)
    {
        for(std::vector< Attribute* >::iterator it = mp_attributes.begin(); it != mp_attributes.end(); ++it)
        {
            (*it)->name = prefixe + (*it)->name;
            //std::cout << (*it)->name << std::endl;
        }
    }

    /// @attention : bien faire attention a changer le nom des attributs avant de les ajouter dans le container (pas apres)
    ///              peut avoir des problemes avec cette fonction a cause de pointeurs
    void AddSuffixe(const std::string suffixe)
    {
        for(std::vector< Attribute* >::iterator it = mp_attributes.begin(); it != mp_attributes.end(); ++it)
        {
            (*it)->name = (*it)->name + suffixe;
        }
    }

    int AddToContainer(Lidar::LidarDataContainer& ldc, enum Attribute::io_style_t io_style)
    {
        if((io_style == Attribute::out) && CheckAttributesArePresent(ldc))
        {
            std::cout << "WARNING: Some attributes are already present so they will not be added" << std::endl;
            return 1;
        }

        if((io_style == Attribute::in) && !CheckAttributesArePresentAndType(ldc))
        {
            std::cout << "WARNING: At least one attribute is missing or has the wrong type" << std::endl;
            return 3;
        }

        for(std::vector< Attribute* >::iterator it = mp_attributes.begin(); it!=mp_attributes.end(); ++it)
        {
            (*it)->AddToContainer(ldc, io_style);
        }
        GetDecalage(ldc);
        m_lidar_data_container = &ldc;
    }

    int GetFromContainer(Lidar::LidarDataContainer& ldc)
    {
        if(!CheckAttributesArePresentAndType(ldc))
        {
            std::cout << "WARNING: At least one attribute is missing or has the wrong type" << std::endl;
            return 3;
        }

        for(std::vector< Attribute* >::iterator it = mp_attributes.begin(); it!=mp_attributes.end(); ++it)
        {
            (*it)->GetFromContainer(ldc);
        }
        GetDecalage(ldc);
        m_lidar_data_container = &ldc;
    }

    /*
            quand on ajoute un attribut au conteneur, le "decalage" obtenu avec la fonction "Lidar::LidarDataContainer::getDecalage()"
            pour les attributs precedement ajoutes devient obsolete,
            setShift recalcule tous les decalages
            */
    void GetDecalage(Lidar::LidarDataContainer& ldc)
    {
        for(std::vector< Attribute* >::iterator it = mp_attributes.begin(); it!=mp_attributes.end(); ++it)
        {
            (*it)->GetDecalage(ldc);
        }
    }


    void Copy(Lidar::LidarDataContainer::iterator it_copy, Lidar::LidarDataContainer::iterator it_model, AttributeList& list_model )
    {

        std::vector< Attribute* >::iterator p_attribute_copy = mp_attributes.begin();
        std::vector< Attribute* >::iterator p_attribute_model = list_model.mp_attributes.begin();

        for(; p_attribute_copy!=mp_attributes.end(); ++p_attribute_copy, ++p_attribute_model)
        {
            if( (*p_attribute_copy)->name == (*p_attribute_model)->name )
            {
                Attribute& attribute_copy =  **p_attribute_copy;
                Attribute& attribute_model = **p_attribute_model;

                attribute_copy.Copy(it_copy,it_model,attribute_model);
            }
        }
    }

    //copy peut contenir un pointeur m_lidar_data_container vers un conteneur vide mais pas un pointeur null
    int Copy(AttributeList& list_model)
    {
        if(m_lidar_data_container==0)
        {
            std::cout << "il faut initialiser le pointeur m_lidar_data_container avec la fonction addToContainer" <<std::endl;
            return 1;
        }

        m_lidar_data_container->resize(list_model.m_lidar_data_container->size());
        GetDecalage( *(m_lidar_data_container) );

        Lidar::LidarDataContainer::iterator it_copy     = m_lidar_data_container->begin();
        Lidar::LidarDataContainer::iterator it_model    = list_model.m_lidar_data_container->begin();

        Lidar::LidarDataContainer::iterator ite = m_lidar_data_container->end();

        for(; it_copy != ite; ++it_copy, ++it_model)
        {
            Copy(it_copy, it_model, list_model);
        }

        return 0;
    }

    //return the index of the attribute in this List
    int AddAttribute(Attribute* attribute)
    {
        mp_attributes.push_back(attribute);
        return mp_attributes.size() - 1;
    }

    void AddAttributeList(AttributeList& attributeList)
    {
        for(std::vector< Attribute* >::iterator it = attributeList.mp_attributes.begin(); it!=attributeList.mp_attributes.end(); ++it)
        {
            mp_attributes.push_back(*it);
        }
    }

    Attribute* At(unsigned int id){return mp_attributes.at(id);}
    Attribute* GetAttribute(const std::string name)
    {
        for(std::vector< Attribute* >::iterator it= mp_attributes.begin(); it!=mp_attributes.end(); ++it)
        {
            if( (**it).name == name )
            {
                //std::cout << name << std::endl;
                return *it;
            }
        }
        return 0;
    }

    std::vector< Attribute* >& Attributes(){return mp_attributes;}

    void GetAttributeList(std::vector<std::string>& v_names)
    {
        v_names.resize(mp_attributes.size());

        std::vector< std::string >::iterator it_name = v_names.begin();

        for(std::vector< Attribute* >::iterator it= mp_attributes.begin(); it!=mp_attributes.end(); ++it, ++it_name)
        {
            *it_name = (**it).name;
        }
    }

    //Delete from container
    void Delete()
    {
        std::vector<std::string> v_names;
        GetAttributeList(v_names);
        m_lidar_data_container->delAttributeList(v_names);
    }

    /*
            void itBegin    (Lidar::LidarDataContainer& ldc){  m_itBegin = ldc.begin();}
            void itEnd      (Lidar::LidarDataContainer& ldc){  m_itEnd = ldc.end();}
            void itPoint    (const unsigned int index){  m_it_point = m_itBegin + index;}
            */

    Lidar::LidarDataContainer::iterator ItBegin    (){  return m_lidar_data_container->begin();}
    Lidar::LidarDataContainer::iterator ItEnd      (){  return m_lidar_data_container->end();}

    Lidar::LidarDataContainer::iterator Begin    (){  return m_lidar_data_container->begin();}
    Lidar::LidarDataContainer::iterator End      (){  return m_lidar_data_container->end();}

    const Lidar::LidarDataContainer::const_iterator Begin    ()const{  return m_lidar_data_container->begin();}
    const Lidar::LidarDataContainer::const_iterator End      ()const{  return m_lidar_data_container->end();}

    Lidar::LidarDataContainer::iterator& itPoint(){return m_it_point;}

    Lidar::LidarDataContainer::iterator itPoint(unsigned int index)const {return m_lidar_data_container->begin() + index;}

    Lidar::LidarDataContainer& LidarDataContainer(){return *m_lidar_data_container;}



};

#endif // __ATTRIBUTE_LIST_H__
