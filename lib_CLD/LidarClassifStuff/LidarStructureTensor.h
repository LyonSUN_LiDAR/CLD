#ifndef LIDAR_STRUCTURE_TENSOR_HPP
#define LIDAR_STRUCTURE_TENSOR_HPP

#include "AttributeList.h"
#include "List/ListCoord.h"
#include "List/ListStructureTensor.h"
#include "List/ListDimensionality.h"
#include "List/ListKNearestNeighbors.h"
#include "List/ListNeighborBounds.h"

#include "Struct/StructureTensor.h"


class LidarStructureTensor
{
public :

        LidarStructureTensor(
                ListCoord& listCoord,
                ListKNearestNeighbors& listKNearestNeighbors,
                ListNeighborBounds& listNeighborBounds,
                ListStructureTensor& listStructureTensor,
                ListDimensionality& listDimensionality):
        m_listNeighborBounds(listNeighborBounds),
        m_listCoord(listCoord),
        m_listKNearestNeighbors(listKNearestNeighbors),
        m_listStructureTensor(listStructureTensor),
        m_listDimensionality(listDimensionality)

        {}

        void runMultiscaleEa();
        void runSimilarity();

        void multiscaleEa();
        void similarity();



    private :

        // trouver la distance du ke voisin au point
        const float distance(Lidar::LidarDataContainer::iterator it_point, const int k);

        int order(const float radiusD1, const float radiusD2, const float radiusD3);

        ListCoord& m_listCoord;
        ListNeighborBounds& m_listNeighborBounds;
        ListKNearestNeighbors& m_listKNearestNeighbors;
        ListStructureTensor& m_listStructureTensor;
        ListDimensionality& m_listDimensionality;
};

        void runGlobalDim(ListCoord& listCoord);


#endif //LIDAR_STRUCTURE_TENSOR_HPP
