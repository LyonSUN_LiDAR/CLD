#ifndef DESCRIPTORS_H
#define DESCRIPTORS_H

#include "Sigmas.h"
#include "Dimensionality.h"
#include "../TAttribute.h"
#include <iostream>
#include "../PList/PListDimensionality.h"
#include "../Struct/StructDimensionality.h"
#include "../PList/PListStructureTensor.h"

template<typename T>
class DimDescriptors : public Dimensionality<T>
{

    T m_d1;
    T m_d2;

public:

    T E1()const{return m_d1;}
    T E2()const{return m_d2;}
    T E3()const{return 1. - m_d1 - m_d2;}

    DimDescriptors(T d1=DEFAULT_DIMENSIONALITY,
                   T d2=DEFAULT_DIMENSIONALITY):
        m_d1(d1),
        m_d2(d2)
    {}

    DimDescriptors(T sigma1, T sigma2, T sigma3)
    {
        const Sigmas<T> sigmas(sigma1, sigma2, sigma3);

        m_d1 = sigmas.E1();
        m_d2 = sigmas.E2();
    }

    DimDescriptors(const Sigmas<T>& sigmas):

        m_d1(sigmas.E1()),
        m_d2(sigmas.E2())
    {
    }

    void Set(const Sigmas<T>& sigmas)
    {
        m_d1 = sigmas.E1();
        m_d2 = sigmas.E2();
    }

        /// @param normalize : pour que les valeurs soient entre 0 et 1
        T EigenEntropy(bool normalize=true)const
        {
            static const T INVERSE_LN3 = 0.910239227; // = 1/ln3

            const T coef = normalize ? INVERSE_LN3 : 1.;

            return 	(   - std::log(std::pow(E1(),E1()))
                        - std::log(std::pow(E2(),E2()))
                        - std::log(std::pow(E3(),E3()))  ) * coef;
        }

        /// @return (entropie > ln2) ou (entropie normalisee > ln2/ln3)
        bool EigenEntropyIsGreat(T EigenEntropy, bool normalized=true)const
        {
            static const T LN2      = 0.693147181; // ln2
            static const T LN2_NORM = 0.630929754; // ln2/ln3

            if(normalized) return EigenEntropy >= LN2_NORM;
            else           return EigenEntropy >= LN2;
        }

        T linearity()const
        {
            if (E1()!=0)
            {return 	((E1() - E2())/E1());}
            else
            {return 0;}
        }


        T planarity()const
        {
            if (E1()!=0)
            {return 	((E2() - E3())/E1());}
            else
            {return 0;}
        }


        T scattering()const
        {
            if (E1()!=0)
            {return 	(E3()/E1());}
            else
            {return 0;}
        }


};

struct StructDimDescriptors : public StructDimensionality
{

    public :
    float D1;
    float D2;
    float D3;
    int k;
    float linearity;
    float planarity;
    float scattering;
    float verticality;

    StructDimDescriptors():
    D1(0),
    D2(0),
    D3(0),
    k(0),
      linearity(0),
      planarity(0),
      scattering(0),
      verticality(0)
    {}


    StructDimDescriptors(
    float _D1,
    float _D2,
    float _D3,
    int _k,
    float _linearity,
    float _planarity,
    float _scattering,
    float _verticality
    ):
    D1(_D1),
    D2(_D2),
    D3(_D3),
    k(_k),
      linearity(_linearity),
      planarity(_planarity),
      scattering(_scattering),
      verticality(_verticality)
    {}

};

class PListDimDescriptors : public PListDimensionality
{
    public :
    TAttribute< float >D1;
    TAttribute< float >D2;
    TAttribute< float >D3;
    TAttribute< int >k;
    TAttribute< float >linearity;
    TAttribute< float >planarity;
    TAttribute< float >scattering;
    TAttribute< float >verticality;

    StructDimDescriptors Get(const Lidar::LidarDataContainer::iterator it)
    {
        return StructDimDescriptors(
        D1.Value(it),
        D2.Value(it),
        D3.Value(it),
        k.Value(it),
        linearity.Value(it),
        planarity.Value(it),
        scattering.Value(it),
        verticality.Value(it)
        );
    }

    void Get(StructDimDescriptors& struct_DimDescriptors, const Lidar::LidarDataContainer::iterator it)
    {
        struct_DimDescriptors.D1 = D1.Value(it);
        struct_DimDescriptors.D2 = D2.Value(it);
        struct_DimDescriptors.D3 = D3.Value(it);
        struct_DimDescriptors.k = k.Value(it);
        struct_DimDescriptors.linearity = linearity.Value(it);
        struct_DimDescriptors.planarity = planarity.Value(it);
        struct_DimDescriptors.scattering = scattering.Value(it);
        struct_DimDescriptors.verticality = verticality.Value(it);
    }

    void Set(const Lidar::LidarDataContainer::iterator it, const StructDimDescriptors& struct_DimDescriptors)
    {
        D1.Value(it) = struct_DimDescriptors.D1;
        D2.Value(it) = struct_DimDescriptors.D2;
        D3.Value(it) = struct_DimDescriptors.D3;
        k.Value(it) = struct_DimDescriptors.k;
        linearity.Value(it) = struct_DimDescriptors.linearity;
        planarity.Value(it) = struct_DimDescriptors.planarity;
        scattering.Value(it) = struct_DimDescriptors.scattering;
        verticality.Value(it) = struct_DimDescriptors.verticality;
    }

    void Set(
        const Lidar::LidarDataContainer::iterator it,
        const float _D1 = 0,
        const float _D2 = 0,
        const float _D3 = 0,
        const int _k = 0,
        const float _linearity = 0,
        const float _planarity = 0,
        const float _scattering = 0,
        const float _verticality = 0
        )
        {
        D1.Value(it) = _D1;
        D2.Value(it) = _D2;
        D3.Value(it) = _D3;
        k.Value(it) = _k;
        linearity.Value(it) = _linearity;
        planarity.Value(it) = _planarity;
        scattering.Value(it) = _scattering;
        verticality.Value(it) = _verticality;
        }

    PListDimDescriptors() : PListDimensionality(),
        D1("D1",0),
        D2("D2",0),
        D3("D3",0),
        k("k",0),
        linearity("linearity",0),
        planarity("planarity",0),
        scattering("scattering",0),
        verticality("verticality",0)
        {
            // BV: removed uncomputed attributes
        AddAttribute(&D1);
        AddAttribute(&D2);
        AddAttribute(&D3);
        AddAttribute(&k);
        AddAttribute(&linearity);
        AddAttribute(&planarity);
        AddAttribute(&scattering);
        AddAttribute(&verticality);
        }
    };

class ListDimDescriptors : public PListDimDescriptors
{
public :

ListDimDescriptors() : PListDimDescriptors(){}

void Set(Lidar::LidarDataContainer::iterator& itPoint, const DimDescriptors<float>& dim)
{
    D1.Value(itPoint) = dim.D1();
    D2.Value(itPoint) = dim.D2();
    D3.Value(itPoint) = dim.D3();
    linearity.Value(itPoint) = dim.linearity();
    planarity.Value(itPoint) = dim.planarity();
    scattering.Value(itPoint) = dim.scattering();
}

void Set(Lidar::LidarDataContainer::iterator& itPoint, const StructDimDescriptors& DimDescriptors)
{
    D1.Value(itPoint) = DimDescriptors.D1;
    D2.Value(itPoint) = DimDescriptors.D2;
    D3.Value(itPoint) = DimDescriptors.D3;
    linearity.Value(itPoint) = DimDescriptors.linearity;
    planarity.Value(itPoint) = DimDescriptors.planarity;
    scattering.Value(itPoint) = DimDescriptors.scattering;
}

};


#endif //DESCRIPTORS_H
