#ifndef __M2_H__
#define __M2_H__

#include <cmath>
#include <iostream>

#include "Vect.h"
#include "CovarianceStorage.h"

template< typename t_value, typename t_weight, int dim>
class M2
{
    unsigned int m_count;

    t_weight m_weight;

    Algo::Vect< t_value, dim> mv_mean;

    CovarianceStorage< t_value, dim> mv_cov;

public :

M2():
        m_count(0),
        m_weight(0)
{}

M2(
        const unsigned int count,
        const t_weight weight,
        const Algo::Vect< t_value, dim>& mean, //&
        const CovarianceStorage< t_value, dim >& covariance): //&
        m_count(count),
        m_weight(weight),
        mv_mean(mean),
        mv_cov(covariance)
{}

M2(const M2& m2):
        m_count(m2.m_count),
        m_weight(m2.m_weight),
        mv_mean(m2.mv_mean),
        mv_cov(m2.mv_cov)
{}

void operator = (const M2& m2)
                {
    m_count     = m2.m_count;
    m_weight    = m2.m_weight;
    mv_mean     = m2.mv_mean;
    mv_cov      = m2.mv_cov;
}

t_weight  Weight() const {return m_weight;}
t_weight& Weight()       {return m_weight;}

unsigned int   Count() const {return m_count;}
unsigned int & Count()       {return m_count;}

t_value  E(const int dimension) const {return mv_mean.at(dimension);}
t_value& E(const int dimension)       {return mv_mean.at(dimension);}

Algo::Vect<t_value, dim>  E() const {return mv_mean;}
Algo::Vect<t_value, dim>& E()       {return mv_mean;}

t_value  Mean(const int dimension) const {return mv_mean.at(dimension);}
t_value& Mean(const int dimension)       {return mv_mean.at(dimension);}

Algo::Vect<t_value, dim>  Mean() const {return mv_mean;}
Algo::Vect<t_value, dim>& Mean()       {return mv_mean;}


CovarianceStorage< t_value, dim >  Covariance() const {return mv_cov;}
CovarianceStorage< t_value, dim >& Covariance()       {return mv_cov;}

t_value  Covariance(const int i, const int j) const {return mv_cov.at(i,j);}
t_value& Covariance(const int i, const int j)       {return mv_cov.at(i,j);}

t_value  Covariance(const int i) const { return mv_cov.at(i); }
t_value& Covariance(const int i)       { return mv_cov.at(i); }

t_value  Variance(const int i) const { return mv_cov.at(i); }
t_value& Variance(const int i)       { return mv_cov.at(i); }

t_value Correlation(const int i, const int j)
{
    const t_value cov_ij = Covariance(i,j);

    if(cov_ij == 0) return 0;

    const t_value var_i = Variance(i);

    if(var_i ==  0) return 0;

    const t_value var_j = Variance(j);

    if(var_j ==  0) return 0;

    if((var_j == var_i) && (cov_ij == var_i))return (t_value) 1;

    return cov_ij /  std::sqrt(var_i * var_j);
}


//Likewise, there is a formula for combining the covariances of two sets that can be used to parallelize the computation:
//C_X = C_A + C_B + (\bar x_A - \bar x_B)(\bar y_A - \bar y_B)\cdot\frac{n_A n_B}{n_X}
M2(const M2& a, const M2& b):
        m_count(a.m_count + b.m_count),
        m_weight(a.m_weight + b.m_weight),
        mv_mean((a.m_weight * a.mv_mean + b.m_weight * b.mv_mean)/m_weight)
{
    t_value d_mean[dim];
    for(int i=0; i!=dim; ++i)
    {
        d_mean[i] = a.mv_mean.at(i) - b.mv_mean.at(i);
    }
    const t_weight prod  = (a.m_weight * b.m_weight) / m_weight;
    const t_weight ratio = (m_weight==0) ? 0 : 1. / m_weight;

    for(int i=0; i!=dim; ++i)
    {
            for(int j=i; j!=dim; ++j)
            {
                mv_cov.Cov(i,j) = ratio * (a.m_weight * a.mv_cov.Cov(i,j) + b.m_weight * b.mv_cov.Cov(i,j) + d_mean[i] * d_mean[j] * prod);
            }
    }
}

    };

#endif //__M2_H__
