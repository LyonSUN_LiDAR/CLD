#ifndef __P_LIST_NEIGHBORBOUNDS_H__
#define __P_LIST_NEIGHBORBOUNDS_H__

#include "PList.h"
#include "../Struct/StructNeighborBounds.h"

class PListNeighborBounds : public PList
{
public :
TAttribute< int >kMin;
TAttribute< int >kMax;
TAttribute< float >radiusMin;
TAttribute< float >radiusMax;

PListNeighborBounds() : PList(),
kMin("kMin",0),
kMax("kMax",0),
radiusMin("radiusMin",0),
radiusMax("radiusMax",0)
{
AddAttribute(&kMin);
AddAttribute(&kMax);
AddAttribute(&radiusMin);
AddAttribute(&radiusMax);
}

StructNeighborBounds Get(const Lidar::LidarDataContainer::iterator it)
{
return StructNeighborBounds(
kMin.Value(it), 
kMax.Value(it), 
radiusMin.Value(it), 
radiusMax.Value(it)
); 
}

void Get(StructNeighborBounds& struct_neighborBounds, const Lidar::LidarDataContainer::iterator it)
{
struct_neighborBounds.kMin = kMin.Value(it);
struct_neighborBounds.kMax = kMax.Value(it);
struct_neighborBounds.radiusMin = radiusMin.Value(it);
struct_neighborBounds.radiusMax = radiusMax.Value(it);
}

void Set(const Lidar::LidarDataContainer::iterator it, StructNeighborBounds& struct_neighborBounds)
{
kMin.Value(it) = struct_neighborBounds.kMin;
kMax.Value(it) = struct_neighborBounds.kMax;
radiusMin.Value(it) = struct_neighborBounds.radiusMin;
radiusMax.Value(it) = struct_neighborBounds.radiusMax;
}

void Set(
const Lidar::LidarDataContainer::iterator it, 
const int _kMin = 0,
const int _kMax = 0,
const float _radiusMin = 0,
const float _radiusMax = 0
)
{
kMin.Value(it) = _kMin;
kMax.Value(it) = _kMax;
radiusMin.Value(it) = _radiusMin;
radiusMax.Value(it) = _radiusMax;
}

};
#endif //__P_LIST_NEIGHBORBOUNDS_H__
