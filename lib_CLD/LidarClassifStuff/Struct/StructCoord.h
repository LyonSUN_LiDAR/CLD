#ifndef __STRUCT_COORD_H__
#define __STRUCT_COORD_H__


struct StructCoord
{
public :
float x;
float y;
float z;

StructCoord():
x(0),
y(0),
z(0)
{}

StructCoord(
float _x,
float _y,
float _z
):
x(_x),
y(_y),
z(_z)
{}

};
#endif //__STRUCT_COORD_H__
