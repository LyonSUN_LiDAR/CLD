#ifndef STRUCTURE_TENSOR_HPP
#define STRUCTURE_TENSOR_HPP

//#include "LfTools/Geom/IntersectionPlane3D.h"

#include "../PCA/PrincipalComponentAnalysis.h"
#include "../PCA/EigenDecomposition.h"
#include "../PCA/Dimensionality.h"

#include "../PCA/Sigmas.h"
#include "../PCA/Tensor3D.h"


#include <fstream>
#include <sstream>

// include headers that implement a archive in simple text format
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/filesystem.hpp>

struct StructStructureTensor
{
    friend class boost::serialization::access;
    // When the class Archive corresponds to an output archive, the
    // & operator is defined similar to <<.  Likewise, when the class Archive
    // is a type of input archive the & operator is defined similar to >>.
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar& gx;
        ar& gy;
        ar& gz;
        ar& lambda1;
        ar& lambda2;
        ar& lambda3;
        ar& sigma1;
        ar& sigma2;
        ar& sigma3;
        ar& eigenVector1x;
        ar& eigenVector1y;
        ar& eigenVector1z;
        ar& eigenVector2x;
        ar& eigenVector2y;
        ar& eigenVector2z;
        ar& eigenVector3x;
        ar& eigenVector3y;
        ar& eigenVector3z;
    }

public :
float gx;
float gy;
float gz;
float lambda1;
float lambda2;
float lambda3;
float sigma1;
float sigma2;
float sigma3;
float eigenVector1x;
float eigenVector1y;
float eigenVector1z;
float eigenVector2x;
float eigenVector2y;
float eigenVector2z;
float eigenVector3x;
float eigenVector3y;
float eigenVector3z;

StructStructureTensor():
gx(0),
gy(0),
gz(0),
lambda1(0),
lambda2(0),
lambda3(0),
sigma1(0),
sigma2(0),
sigma3(0),
eigenVector1x(0),
eigenVector1y(0),
eigenVector1z(0),
eigenVector2x(0),
eigenVector2y(0),
eigenVector2z(0),
eigenVector3x(0),
eigenVector3y(0),
eigenVector3z(0)
{}

StructStructureTensor(
float _gx,
float _gy,
float _gz,
float _lambda1,
float _lambda2,
float _lambda3,
float _sigma1,
float _sigma2,
float _sigma3,
float _eigenVector1x,
float _eigenVector1y,
float _eigenVector1z,
float _eigenVector2x,
float _eigenVector2y,
float _eigenVector2z,
float _eigenVector3x,
float _eigenVector3y,
float _eigenVector3z
):
gx(_gx),
gy(_gy),
gz(_gz),
lambda1(_lambda1),
lambda2(_lambda2),
lambda3(_lambda3),
sigma1(_sigma1),
sigma2(_sigma2),
sigma3(_sigma3),
eigenVector1x(_eigenVector1x),
eigenVector1y(_eigenVector1y),
eigenVector1z(_eigenVector1z),
eigenVector2x(_eigenVector2x),
eigenVector2y(_eigenVector2y),
eigenVector2z(_eigenVector2z),
eigenVector3x(_eigenVector3x),
eigenVector3y(_eigenVector3y),
eigenVector3z(_eigenVector3z)
{}

void Transform(float& x, float& y, float& z)const
{
    Center(x, y, z);
    Rotation(x, y, z);
    Homothety(x, y, z);
}

void InverseTransform(float& x, float& y, float& z)const
{
    InverseHomothety(x, y, z);
    InverseRotation(x, y, z);
    InverseCenter(x, y, z);
}

void Homothety(float& x, float& y, float& z)const
{
   if(sigma1 > 0) x /= sigma1;
   if(sigma2 > 0) y /= sigma2;
   if(sigma3 > 0) z /= sigma3;
}

void InverseHomothety(float& x, float& y, float& z)const
{
   x *= sigma1;
   y *= sigma2;
   z *= sigma3;
}

void Center(float& x, float& y, float& z)const
{
    x -= gx;
    y -= gy;
    z -= gz;
}

void InverseCenter(float& x, float& y, float& z)const
{
    x += gx;
    y += gy;
    z += gz;
}

void Rotation(float& x, float& y, float& z)const
{
    const float vect_x = x;
    const float vect_y = y;
    const float vect_z = z;

    x = vect_x * eigenVector1x  + vect_y * eigenVector1y + vect_z * eigenVector1z;
    y = vect_x * eigenVector2x  + vect_y * eigenVector2y + vect_z * eigenVector2z;
    z = vect_x * eigenVector3x  + vect_y * eigenVector3y + vect_z * eigenVector3z;
}

void InverseRotation(float& x, float& y, float& z)const
{
    const float vect_1 = x;
    const float vect_2 = y;
    const float vect_3 = z;

    x = vect_1 * eigenVector1x  + vect_2 * eigenVector2x + vect_3 * eigenVector3x;
    y = vect_1 * eigenVector1y  + vect_2 * eigenVector2y + vect_3 * eigenVector3y;
    z = vect_1 * eigenVector1z  + vect_2 * eigenVector2z + vect_3 * eigenVector3z;
}

void Save(StructStructureTensor& structure_tensor, const std::string & filename)
{
    std::ofstream ofs(const_cast<char*>(filename.c_str()));

    boost::archive::text_oarchive oa(ofs);
    // write class instance to archive
    oa << structure_tensor;
    // archive and stream closed when destructors are called
}

void Restore(StructStructureTensor& structure_tensor, const std::string & filename)
{
    // ... some time later restore the class instance to its orginal state

    std::cout << filename << std::endl;

    // create and open an archive for input
    std::ifstream ifs(const_cast<char*>(filename.c_str()));
    boost::archive::text_iarchive ia(ifs);
    // read class state from archive
    ia >> structure_tensor;
    // archive and stream closed when destructors are called
}

};

class StructureTensor
{

public :
    StructureTensor():
        m_eigen(3)
    {}

    ~StructureTensor(){}
    void operator()(const float x, const float y, const float z, const float weight=1)
    {
        if(m_pcaXYZ.nbObs() == 0) m_first.SetXYZ(x,y,z);

        m_last.SetXYZ(x,y,z);

        m_pcaXYZ.push(x,y,z,weight);
    }

    //Likewise, there is a formula for combining the covariances of two sets that can be used to parallelize the computation:
    //C_X = C_A + C_B + (\bar x_A - \bar x_B)(\bar y_A - \bar y_B)\cdot\frac{n_A n_B}{n_X}.



    void Clear(){m_pcaXYZ = PrincipalComponentAnalysis<float,float,3>();}

    float gx(){ return m_pcaXYZ.moments().E(0);}
    float gy(){ return m_pcaXYZ.moments().E(1);}
    float gz(){ return m_pcaXYZ.moments().E(2);}


    StructStructureTensor GetStruct()
    {
        StructStructureTensor structure;

        structure.gx = gx();
        structure.gy = gy();
        structure.gz = gz();

        structure.lambda1 = m_eigen.Lambda1();
        structure.lambda2 = m_eigen.Lambda2();
        structure.lambda3 = m_eigen.Lambda3();

        structure.sigma1 = m_sigmas.Sigma1();
        structure.sigma2 = m_sigmas.Sigma2();
        structure.sigma3 = m_sigmas.Sigma3();

        structure.eigenVector1x = m_eigen.E1X();
        structure.eigenVector1y = m_eigen.E1Y();
        structure.eigenVector1z = m_eigen.E1Z();
        structure.eigenVector2x = m_eigen.E2X();
        structure.eigenVector2y = m_eigen.E2Y();
        structure.eigenVector2z = m_eigen.E2Z();
        structure.eigenVector3x = m_eigen.E3X();
        structure.eigenVector3y = m_eigen.E3Y();
        structure.eigenVector3z = m_eigen.E3Z();

        return structure;
    }

    template<typename U>
    void Get(Tensor3D<U>& tensor3d)
    {
        tensor3d = Tensor3D<U>(m_eigen.G <Lg::TPoint3<U> >(),
                               m_eigen.E1<Lg::TPoint3<U> >(),
                               m_eigen.E2<Lg::TPoint3<U> >(),
                               m_eigen.E3<Lg::TPoint3<U> >(),
                               m_sigmas.Sigma1(),
                               m_dim.D1(),
                               m_dim.D2(),
                               m_pcaXYZ.nbObs()
                               );
    }

    void FindEigen()
    {
        if(m_pcaXYZ.isVoid())
        {
            std::cout << "PCA is void." << std::endl;
            return;
        }
        else
        {
            m_eigen = EigenDecomposition<Matrix3f,Vector3f>(m_pcaXYZ);

            m_sigmas.SetFromLambdas(m_eigen.Lambda1(), m_eigen.Lambda2(), m_eigen.Lambda3());

            m_dim.Set(m_sigmas);
        }
    }

    const PrincipalComponentAnalysis<float,float,3>& GetPrincipalComponentAnalysis(){ return m_pcaXYZ;}
    const Sigmas<float>& GetSigmas()const{return m_sigmas;}
    const Dimensionality<float>& GetDim()const{return m_dim;}
    const EigenDecomposition<Matrix3f,Vector3f>& GetEigenDecomposition(){return m_eigen;}
    const EigenDecomposition<Matrix3f,Vector3f>& Eigen(){return m_eigen;}

private :
    PrincipalComponentAnalysis<float,float,3> m_pcaXYZ;
    EigenDecomposition<Matrix3f,Vector3f> m_eigen;
    Sigmas<float> m_sigmas;
    Dimensionality<float> m_dim;

    Lg::Point3f m_first, m_last;
};

#endif //STRUCTURE_TENSOR_HPP
