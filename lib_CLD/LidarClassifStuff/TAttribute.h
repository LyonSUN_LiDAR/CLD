#ifndef __TATTRIBUTE_H__
#define __TATTRIBUTE_H__

#include <vector>

#include <typeinfo>

#include <LidarFormat/LidarDataContainer.h>
#include <LidarFormat/LidarDataFormatTypes.h>
#include <LidarFormat/LidarFile.h>
#include <LidarFormat/LidarEcho.h>

#include "Attribute.h"

#include "Condition.h"

#include "MinMax.h"

template< typename Type_t>
struct TAttribute : public Attribute
{
public:
    Type_t default_value;
    Type_t min;
    Type_t max;

    TAttribute(const std::string arg_name="",const Type_t _default_value=0):
        Attribute(arg_name, GetType(),0),
        default_value(_default_value)
    {}

    Lidar::LidarDataType GetType()
    {
        if(typeid(Type_t) == typeid(Lidar::float32) ) return Lidar::LidarDataType::float32;
        else if(typeid(Type_t) == typeid(Lidar::float64) ) return Lidar::LidarDataType::float64;
        else if(typeid(Type_t) == typeid(Lidar::int64) ) return Lidar::LidarDataType::int64;
        else if(typeid(Type_t) == typeid(Lidar::int8) ) return Lidar::LidarDataType::int8;
        else if(typeid(Type_t) == typeid(Lidar::int16) ) return Lidar::LidarDataType::int16;
        else if(typeid(Type_t) == typeid(Lidar::int32) ) return Lidar::LidarDataType::int32;
        else if(typeid(Type_t) == typeid(Lidar::int64) ) return Lidar::LidarDataType::int64;
        else if(typeid(Type_t) == typeid(Lidar::uint8) ) return Lidar::LidarDataType::uint8;
        else if(typeid(Type_t) == typeid(Lidar::uint16) ) return Lidar::LidarDataType::uint16;
        else if(typeid(Type_t) == typeid(Lidar::uint32) ) return Lidar::LidarDataType::uint32;
        else if(typeid(Type_t) == typeid(Lidar::uint64) ) return Lidar::LidarDataType::uint64;
    }

    void Fill(Lidar::LidarDataContainer& ldc,const Type_t value)
    {
        //std::cout << "Fill" << std::endl;
        std::fill(ldc.beginAttribute<Type_t>(name), ldc.endAttribute<Type_t>(name), value);
    }

    Type_t& Value(Lidar::LidarEcho & echo) {return echo.value< Type_t >(shift);}

    Type_t Value(const Lidar::LidarEcho & echo) const {return echo.value< Type_t >(shift);}

    Type_t& Value(Lidar::LidarDataContainer::iterator it) {return it.value< Type_t >(shift);}

    Type_t Value(Lidar::LidarDataContainer::iterator it) const {return it.value< Type_t >(shift);}

    /*
        void addToList(AttributeList& attributeList)
        {
                attributeList.addAttribute( this );
        }
        */

    void FillDefaultValue(Lidar::LidarDataContainer& ldc)
    {
        this->Fill(ldc,default_value);
    }

    //Ecrase, meme si l'attribut existait deja
    void SetDefaultValue(Lidar::LidarDataContainer::iterator it)
    {
        Value(it) = default_value;
    }

    void Copy(Lidar::LidarDataContainer::iterator copy, Lidar::LidarDataContainer::iterator model)
    {
        Value(copy) = Value(model);
    }

    void Copy(Lidar::LidarDataContainer& ldc, TAttribute< Type_t >& attribute_model)
    {
        Copy(ldc,ldc,attribute_model);
    }

    void Copy(Lidar::LidarDataContainer& ldc_copy, Lidar::LidarDataContainer& ldc_model, TAttribute< Type_t >& attribute_model)
    {
        const Lidar::LidarDataContainer::iterator it_copyEnd = ldc_copy.end();
        const Lidar::LidarDataContainer::iterator it_model_end = ldc_model.end();

        Lidar::LidarDataContainer::iterator it_copy = ldc_copy.begin();
        Lidar::LidarDataContainer::iterator it_model = ldc_model.begin();

        for(;it_copy!=it_copyEnd, it_model!=it_model_end; ++it_copy, ++it_model)
        {
            Value(it_copy) = attribute_model.Value(it_model);
        }

    }

    void Copy(Lidar::LidarDataContainer::iterator it, TAttribute< Type_t >& attribute_model)
    {
        Copy(it, it, attribute_model);
    }

    void Copy(Lidar::LidarDataContainer::iterator it_copy, Lidar::LidarDataContainer::iterator it_model, TAttribute< Type_t >& attribute_model)
    {
        Value(it_copy) = attribute_model.Value(it_model);
    }

    //n'ecrase pas si l'attribut existait deja
    void SetDefaultValue(Lidar::LidarDataContainer& ldc, const bool FORCE=false)
    {
        if( FORCE || !ldc.checkAttributeIsPresent(name) )
        {
            //std::cout << "L'attribut "<< this->name << " est nouveau. Remplissage avec la valeur " << this->default_value << std::endl;
            this->CheckName();
            this->AddToContainer(ldc);
            FillDefaultValue(ldc);
        }
    }

    void FindMinMax(//Lidar::LidarDataContainer& ldc,
                    const Lidar::LidarDataContainer::iterator itb,
                    const Lidar::LidarDataContainer::iterator ite)
    {
        //this->shift = ldc.getDecalage(this->name);

        min = itb.value<Type_t>(this->shift);
        max = itb.value<Type_t>(this->shift);
        for(Lidar::LidarDataContainer::iterator it = itb; it != ite; ++it)
        {
            min = std::min<Type_t>(min,it.value<Type_t>(this->shift));
            max = std::max<Type_t>(max,it.value<Type_t>(this->shift));
        }
    }

    void FindMinMax(Lidar::LidarDataContainer& ldc)
    {
        FindMinMax(ldc.begin(), ldc.end());
    }

    /// @brief retourne l'iterator du point ayant la valeur la plus proche de la valeur @value passee en parametre
    Lidar::LidarDataContainer::iterator Closest(
            const Lidar::LidarDataContainer::iterator itb,
            const Lidar::LidarDataContainer::iterator ite,
            const Type_t value)const
    {
        //this->shift = ldc.getDecalage(this->name);
        Algo::Min<Type_t, Lidar::LidarDataContainer::iterator> min_distance;

        for(Lidar::LidarDataContainer::iterator it = itb; it != ite; ++it)
        {
            min_distance(fabs(Value(it) - value), it);
        }

        return min_distance.Id();
    }

    void Bound(Lidar::LidarDataContainer& ldc)
    {
        const Lidar::LidarDataContainer::iterator ite = ldc.end();
        const Lidar::LidarDataContainer::iterator itb = ldc.begin();

        for(Lidar::LidarDataContainer::iterator it = itb; it != ite; ++it)
        {
            it.value<Type_t>(this->shift) = std::max<Type_t>(min,it.value<Type_t>(this->shift));
            it.value<Type_t>(this->shift) = std::min<Type_t>(max,it.value<Type_t>(this->shift));
        }
    }

    const bool IsBetweenBounds(Lidar::LidarDataContainer::iterator it)
    {
        Type_t value = Value(it);
        return (min <= value) && (value < max);
    }

    //copie tous les attributs du container dans le nouveau conteneur
    //seul les points pour lesquels this.value == true sont copies

    // exemple de condition : Lf::IsTrue()

    template<class t_condition>
    void KeepIf(Lidar::LidarDataContainer& ldc_copy, Lidar::LidarDataContainer& ldc_model, t_condition condition)
    {
        AddToContainer(ldc_model);
        //std::cout << "Keep points if " << name << ".Value respects contdition" << std::endl;
        std::cout << "Size in = " << ldc_model.size() << std::endl;

        ldc_copy = Lidar::LidarDataContainer(ldc_model);

        std::vector< std::string > v_attribute;
        ldc_model.getAttributeList(v_attribute);

        const Lidar::LidarDataContainer::iterator it_model_end = ldc_model.end();

        Lidar::LidarDataContainer::iterator it_model;
        Lidar::LidarDataContainer::iterator it_copy;

        int new_size = 0;
        for(it_copy = ldc_copy.begin(), it_model = ldc_model.begin();it_model!=it_model_end; ++it_model)
        {
            if(condition(Value(it_model)))
            {
                *it_copy = *it_model;
                ++it_copy;
                ++new_size;
            }
        }
        //ldc_copy.delAttribute(this->name);
        ldc_copy.resize(new_size);
        std::cout << "KeepIf: " << new_size << "/" << ldc_model.size() << "=" <<
                     100.*new_size/(double)ldc_model.size() << "%" << std::endl;
    }

    void KeepIf(Lidar::LidarDataContainer& ldc_copy, Lidar::LidarDataContainer& ldc_model)
    {
        KeepIf(ldc_copy, ldc_model, Lf::IsTrue());
    }
};


#endif // __TATTRIBUTE_H__
